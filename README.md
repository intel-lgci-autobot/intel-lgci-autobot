# Intel Linux Graphics CI Autobot

- This account is used for access/CI integration with Intel Graphics projects on fd.o GitLab.
- Owners: @dragoonaethis and @mramotow
- Service Account: sys_igkgfxci
